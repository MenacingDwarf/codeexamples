﻿#pragma once

#include <CoreMinimal.h>
#include <Kismet/BlueprintFunctionLibrary.h>

#include "OnlineSubsystemEOSLibrary.generated.h"

class IVoiceChatUser;

UCLASS()
class ONLINESUBSYSTEMEOS_API UOnlineSubsystemEOSLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintPure, Category = "OnlineSubsystemEOS", meta = (WorldContext = "WorldContextObject"))
	static FString GetProductUserIdStr(UObject* WorldContextObject, const FString& NetId);

	static IVoiceChatUser* GetLocalVoiceChatUserInterface(const UWorld* World);
};
