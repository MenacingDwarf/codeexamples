#pragma once

#include "CoreMinimal.h"
#include "Containers/ArrayView.h"
#include "Components/AudioComponent.h"
#include "EOSVoiceChatAudioComponent.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogSpatialVoice, Log, All);

class USoundWaveProcedural;

/**
 * VoiceChatAudioComponent should be added to the Character to make its voice chat spatial
 */
UCLASS()
class MYPROJECT_API UEOSVoiceChatAudioComponent : public UAudioComponent
{
	GENERATED_BODY()

	UEOSVoiceChatAudioComponent(const FObjectInitializer& ObjectInitializer);

protected:
	// Begin override UActorComponent
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	// End override UActorComponent

	void InitWithEosNetId(const FString& EosNetId);
	
	void OnBeforeVoiceRender(TArrayView<int16> PcmSamples, int SampleRate, int Channels, bool bIsSilence, const FString& ChannelName, const FString& PlayerName);
	void FillAudio(USoundWaveProcedural* Wave, const int32 SamplesNeeded);
	
private:
	UPROPERTY(Transient)
	USoundWaveProcedural* SoundStream = nullptr;
	
	FCriticalSection BeforeRecvAudioRenderedLock;
	FDelegateHandle OnBeforeVoiceRenderDelegate;
	FTimerHandle ManualClearFramesTimer;
	
	FString ProductUserId;
	TArray<int16> FramesBuffer;
	
	uint32 EosSamplesRate = 16000;
	int32 EosNumChannels = 1;
};
