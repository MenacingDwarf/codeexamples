#include "GameplayFramework/Characters/EOSVoiceChatAudioComponent.h"

#include "OnlineSubsystemEOSLibrary.h"
#include "VoiceChat.h"
#include "Sound/SoundWaveProcedural.h"

#include "GameplayFramework/Characters/VRCharacterBase.h"
#include "GameplayFramework/PlayerStates/GameplayPlayerStateBase.h"

DEFINE_LOG_CATEGORY(LogSpatialVoice);

UEOSVoiceChatAudioComponent::UEOSVoiceChatAudioComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// these values are hardcoded based on information from eos
	// if there are any strange sounds or voice chat stops working, please check if the numbers are correct
#if PLATFORM_ANDROID
	EosSamplesRate = 16000;
#else
	EosSamplesRate = 48000;
#endif
}

void UEOSVoiceChatAudioComponent::BeginPlay()
{
	Super::BeginPlay();

	const ACharacter* OwnerCharacter = Cast<ACharacter>(GetOwner());
	if (!OwnerCharacter || OwnerCharacter->IsLocallyControlled()) // we don't need voice from local player
	{
		return;
	}

	const APlayerState* PlayerState = OwnerCharacter->GetPlayerState();
	if (!PlayerState)
	{
		return;
	}

	const FString EosNetId = PlayerState->GetUniqueId().ToString();
	InitWithEosNetId(EosNetId);
}

void UEOSVoiceChatAudioComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	
	IVoiceChatUser* VoiceChatUser = UOnlineSubsystemEOSLibrary::GetLocalVoiceChatUserInterface(GetWorld());
	if (!VoiceChatUser || !OnBeforeVoiceRenderDelegate.IsValid())
	{
		return;
	}

	VoiceChatUser->UnregisterOnVoiceChatBeforeRecvAudioRenderedDelegate(OnBeforeVoiceRenderDelegate);
}

void UEOSVoiceChatAudioComponent::InitWithEosNetId(const FString& EosNetId)
{
	if (EosNetId.IsEmpty())
	{
		return;
	}
	
	ProductUserId = UOnlineSubsystemEOSLibrary::GetProductUserIdStr(this, EosNetId);
	UE_LOG(LogSpatialVoice, Log, TEXT("[UVoiceChatAudioComponent::InitWithEosNetId] ProductUserId=%s"), *ProductUserId);

	if (!SoundStream)
	{
		// create and play sound
		SoundStream = NewObject<USoundWaveProcedural>();
		SoundStream->SetSampleRate(EosSamplesRate);
		SoundStream->NumChannels = EosNumChannels;
		SoundStream->Duration = INDEFINITELY_LOOPING_DURATION;
		SoundStream->SoundGroup = SOUNDGROUP_Default;
		SoundStream->bLooping = false;

		SoundStream->OnSoundWaveProceduralUnderflow.BindLambda
			  ([this](USoundWaveProcedural* Wave, const int32 SamplesNeeded) {
				  FillAudio(Wave, SamplesNeeded);
			  });

		SetSound(SoundStream);
		Play();

		// bind eos sound events
		IVoiceChatUser* VoiceChatUser = UOnlineSubsystemEOSLibrary::GetLocalVoiceChatUserInterface(GetWorld());
		if (!VoiceChatUser)
		{
			return;
		}

		OnBeforeVoiceRenderDelegate = VoiceChatUser->RegisterOnVoiceChatBeforeRecvAudioRenderedDelegate(
		FOnVoiceChatBeforeRecvAudioRenderedDelegate::FDelegate::CreateLambda([this](TArrayView<int16> PcmSamples, int SampleRate, int Channels,
											   bool bIsSilence, const FString& ChannelName, const FString& PlayerName)
		{
			OnBeforeVoiceRender(PcmSamples, SampleRate, Channels, bIsSilence, ChannelName, PlayerName);
		}));
	}
}

void UEOSVoiceChatAudioComponent::OnBeforeVoiceRender(TArrayView<int16> PcmSamples, int SampleRate, int Channels,
                                                   bool bIsSilence, const FString& ChannelName, const FString& PlayerName)
{
	UE_LOG(LogSpatialVoice, Verbose, TEXT("[UVoiceChatAudioComponent::OnBeforeVoiceRender] PlayerName=%s, SampleRate=%i, Channels=%i"), *PlayerName, SampleRate, Channels);
	if (PlayerName.IsEmpty() || ProductUserId != PlayerName || SampleRate != EosSamplesRate || Channels != EosNumChannels)
	{
		return;
	}
	
	FScopeLock Lock(&BeforeRecvAudioRenderedLock);
	FramesBuffer.Append(PcmSamples);
	UE_LOG(LogSpatialVoice, Verbose, TEXT("[UVoiceChatAudioComponent::OnBeforeVoiceRender] Append %i frames to buffer"), PcmSamples.Num());
	
	// zero all samples to prevent eos sound play
	// todo: check performance impact
	for (int32 SampleIndex = 0; SampleIndex < PcmSamples.Num(); SampleIndex++)
	{
		int16* Sample = PcmSamples.GetData() + SampleIndex;
		*Sample = 0;
	}
}

void UEOSVoiceChatAudioComponent::FillAudio(USoundWaveProcedural* Wave, const int32 SamplesNeeded)
{
	UE_LOG(LogSpatialVoice, Verbose, TEXT("[UVoiceChatAudioComponent::FillAudio] SamplesNeeded=%i"), SamplesNeeded);
	if (!SoundStream)
	{
		return;
	}
	
	// Unreal engine uses a fixed sample size.
	constexpr uint32 SampleSize = sizeof(uint16);
	
	FScopeLock Lock(&BeforeRecvAudioRenderedLock);
	const uint32 SampleCount = FMath::Min<uint32>(FramesBuffer.Num(), SamplesNeeded);
	const int16* FramesData = FramesBuffer.GetData();
	SoundStream->QueueAudio(reinterpret_cast<const uint8*>(FramesData), SampleCount * SampleSize);
	FramesBuffer.Empty(1024); // maybe it will be better to leave small buffer?
	UE_LOG(LogSpatialVoice, Verbose, TEXT("[UVoiceChatAudioComponent::FillAudio] Frames=%i, LeftFrames=%i"), SampleCount, FramesBuffer.Num());
}
