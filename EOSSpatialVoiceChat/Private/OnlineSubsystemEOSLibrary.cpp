﻿#include "OnlineSubsystemEOSLibrary.h"

#include "OnlineSubsystemEOS.h"
#include "OnlineSubsystemUtils.h"
#include "UserManagerEOS.h"
#include "EOSVoiceChatUser.h"

FString UOnlineSubsystemEOSLibrary::GetProductUserIdStr(UObject* WorldContextObject, const FString& NetId)
{
	const IOnlineSubsystem* OnlineSubsystem = Online::GetSubsystem(WorldContextObject->GetWorld(), EOS_SUBSYSTEM);
	if (OnlineSubsystem)
	{
		const IOnlineIdentityPtr IdentityInterface = OnlineSubsystem->GetIdentityInterface();

		if (IdentityInterface)
		{
			const FUniqueNetIdEOS UniqueNetIdEos = FUniqueNetIdEOS::Cast(*IdentityInterface->CreateUniquePlayerId(NetId));

			return LexToString(UniqueNetIdEos.GetProductUserId());
		}
	}

	return "";
}

IVoiceChatUser* UOnlineSubsystemEOSLibrary::GetLocalVoiceChatUserInterface(const UWorld* World)
{
	if (!World)
	{
		return nullptr;
	}

	FOnlineSubsystemEOS* OnlineSubsystemEOS = static_cast<FOnlineSubsystemEOS*>(Online::GetSubsystem(World, EOS_SUBSYSTEM));

	if(!OnlineSubsystemEOS)
	{
		return nullptr;
	}

	const FUniqueNetIdPtr UniqueNetIdPtr = OnlineSubsystemEOS->GetIdentityInterface()->GetUniquePlayerId(0);
	if (!UniqueNetIdPtr)
	{
		return nullptr;
	}

	return OnlineSubsystemEOS->GetVoiceChatUserInterface(*UniqueNetIdPtr);
}