# EOS Spatial Voice Chat

In this example, you can see how I made the EOS voice chat spatial. EOS provides us with an interface that gives us the ability to read and modify incoming PCM voice samples before they are rendered. We can use this information to pass audio information to the audio component for spatial audio. We also need to zero out all samples after that to prevent EOS from playing voice sound.

This component should be used on a ACharacter. But I also needed to make some changes to the engine to get it to work:

1. In _\Engine\Plugins\Online\VoiceChat\EOSVoiceChat\Source\EOSVoiceChat\Private\EOSVoiceChatUser.cpp_ we need to make `AudioBeforeRenderOptions.bUnmixedAudio = true` in BindChannelCallbacks function. We need this to receive sound information from each user separately.
2. I had to create my own library UOnlineSubsystemEOOSLibrary in OnlineSubsystemEOS to be able to use some private classes. There are two uses for this library in my code: to get the ProductUserId from the EOSNetId, and to get the local voice chat UI. You should place code for this library in _\Engine\Plugins\Online\OnlineSubsystemEOS\Source\OnlineSubsystemEOS_
